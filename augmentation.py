
images_directory = './data/output'
image_name = 'flaier_augmentation'


#remove dir if exist:
import shutil 
try:
        shutil.rmtree(images_directory)
except:
        pass

#create image augmentation dataset
import Augmentor
p = Augmentor.Pipeline("./data/")

p.rotate(probability=0.5, max_left_rotation=15, max_right_rotation=15)
p.zoom(probability=0.5, min_factor=1.1, max_factor=1.6)
p.random_erasing(probability=0.5, rectangle_area=0.2)
p.random_erasing(probability=0.1, rectangle_area=0.6)
p.scale(probability=0.2, scale_factor = 2)
p.rotate_without_crop(probability =1, max_left_rotation = 15, max_right_rotation = 15, expand=True)
#p.gaussian_distortion(probability=0.5, grid_width = 10, grid_height=10, magnitude=5, corner="bell", method="out", mex=0.5, mey=0.5,sdx=0.05, sdy=0.05)
p.greyscale(probability=0.1)
p.flip_random(probability=0.1)
p.histogram_equalisation(probability=0.5)
p.invert(probability=0.2)

p.sample(400)


#change the names for all of thee images
def changeAllImagesName(name):
        import os
        i = 0

        for filename in os.listdir(images_directory): 
                if filename.endswith(".jpg"):
                        dst = name + str(i) + ".jpg"
                        src = images_directory + "/" + filename 
                        dst = images_directory + "/" + dst
                                
                        # rename() function will 
                        # rename all the files 
                        os.rename(src, dst) 
                        i += 1
                        
changeAllImagesName(image_name)

#create descriptions files for yolo:
import os
images = []
for filename in os.listdir(images_directory):
    if filename.endswith(".jpg"):
        images.append(filename.split('.')[0])
        print(filename.split('.')[0])
        continue
    else:
        continue


#create .txt files for yolo        
import os

out_directory = 'C:/Users/barak/OneDrive/Desktop/Projects/tmp/image-detection/data/output/'
if not os.path.exists(out_directory):
    os.makedirs(out_directory)

for image_name in images:
    with open(out_directory + image_name + '.txt', 'w+') as f:
        f.write("1 0.5 0.5 0.9 0.9")


#Create file train.txt in directory build\darknet\x64\data\, with filenames of your images, each filename in new line, with path relative to darknet.exe
import os
out_directory = 'C:/Users/barak/OneDrive/Desktop/Projects/tmp/image-detection/data/output/'
for image_name in images:
    with open(out_directory + 'train.txt', 'a+') as f:
        f.write('data/obj/' + image_name + '.jpg\n')
