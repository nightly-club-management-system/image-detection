import os
images_directory = './data/output'

def changeAllImagesName(name):
        i = 0

        for filename in os.listdir(images_directory): 
                dst = name + str(i) + ".jpg"
                src = images_directory + "/" + filename 
                dst = images_directory + "/" + dst
                        
                # rename() function will 
                # rename all the files 
                os.rename(src, dst) 
                i += 1
                
changeAllImagesName("flaier_augmentation")